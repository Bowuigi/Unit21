# Unit21 (Formerly Userspace Programs Replacement Project)

Collection of user friendly programs that aim for simplicity and "doing the right thing" by default

The goal of the project is to provide an entire userspace with nothing more than POSIX C99 compatibility.

# Clone this repository

If you want to develop or build from source, you need to clone recursively clone the repository since it contains git submodules

```sh
git clone --recurse-submodules https://codeberg.org/Bowuigi/Unit21
```

# Currently done:

- Shared/colors (library): Handle ANSI escape secuences and choose when to use them
- Management/user (program): Get information about an user or a list of them
- Management/group (program): Get information about a group or a list of them
- Utils/xv (program): View files of any type in hexadecimal
- Management/boas (program): Execute programs as root
- Shared/msg (library): Context based messages sent to stderr
- Shared/parsing (library): Very simple utilities to parse and do operations on strings
- Shared/c3g (library): Use C3 as a configuration file format

# In progress

- Management/mop (program): Manage packages with it or with the exposed directory interface
- Games/abb (program): ASCII Bot Battle, fight with bots with special abilities on a text based arena.
- Utils/nx (program): Ultimate planning solution, or at least the base of it.

# Programs that will most likely be here:

- Text manipulation programs using the capabilities of the shell and some form of structural Regex (My pattern matching library, Match, could help)

- Simple file management programs (like the usual `ls`, `cp`, `mv`, `rm`)

- Process monitoring (`top`, `ps`, `kill`, `pgrep`, `pkill`, `killall`)

- A text editor with syntax highlighting using mainly structural Regex

- A shell (Very simple, pipes and redirections only)

- System management utilities (socket inspector, user and group management)

- Service manager (Like Runit)

- Login manager

- A diff that uses structural Regex to show what part of a file changed, and not just the lines, like Rob Pike proposed

- Utilities for shell scripting (`test`, a testing framework)

- Utilities for planning the future (nx covers this entirely)

- Extras (JSON conversion, a syntax highlighter, simple web browser, a cooler `who`)

- Games (Roguelikes, incrementals, or something like that)

Everything should work on the console, preferably being non-interactive to be very compatible with scripts and multiple forms of user interfaces (like Plan9's text based interfaces, FUSE filesystems, GUIs, and more).

The programs should be written in POSIX C99 style (newer functions are allowed, newer features are not) and have manpages

