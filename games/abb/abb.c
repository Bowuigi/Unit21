/*
 * ABB: ASCII Bot Battle
*/

#include "abb.h"

const Equipment armors[] = {
	{.name = "Bluesteel armor",
		.speed = -1,
		.health = +4,
		.attack = +2,
		.resistance = {0},
		.bonus = {0}
	},
	{.name = "Thunderstone armor",
		.speed = +2,
		.health = +1,
		.attack = +7,
		.resistance = {.electricity = +10, .melee = +2},
		.bonus = {.electricity = +10, .melee = +2}
	}
};

size_t armors_length = sizeof(armors)/sizeof(armors[0]);

const Floor floors[] = {
	{.name = "Dirt",                .speed = +0, .hurt = {0}},
	{.name = "Concrete",            .speed = +2, .hurt = {0}},
	{.name = "Asphalt",             .speed = +4, .hurt = {0}},
	{.name = "Water",               .speed = -4, .hurt = {.water       = 10}},
	{.name = "Lava",                .speed = -2, .hurt = {.fire        = 10}},
	{.name = "Conductive flooring", .speed = +0, .hurt = {.electricity = 10}},
	{.name = "Spikes",              .speed = +0, .hurt = {.melee       = 10}}
};

size_t floors_length = sizeof(floors)/sizeof(floors[0]);

#define DIRT_FLOOR floors[0]
#define CONCRETE_FLOOR floors[1]
#define ASPHALT_FLOOR floors[2]
#define WATER_FLOOR floors[3]
#define LAVA_FLOOR floors[4]
#define CONDUCTIVE_FLOOR floors[5]
#define SPIKE_FLOOR floors[6]

void DrawMap(Map map) {
	int x, y;
	for (y = 0; y < ZONE_HEIGHT; y++) {
		for (x = 0; x < ZONE_WIDTH; x++) {
			fputs(map.top[x * y].name, stdout);
			putchar(' ');
		}
		fputs("\n", stdout);
	}

	putchar('\n');

	for (x = 0; x < ZONE_WIDTH; x++) {
		fputs(map.middle[x].name, stdout);
		putchar(' ');
	}

	putchar('\n');
	putchar('\n');

	for (y = 0; y < ZONE_HEIGHT; y++) {
		for (x = 0; x < ZONE_WIDTH; x++) {
			fputs(map.bottom[x * y].name, stdout);
			putchar(' ');
		}
		fputs("\n", stdout);
	}
}

char *get_class(BotClass type) {
	switch (type) {
		case BOT_FIRE:
			return "Fire";
		case BOT_WATER:
			return "Water";
		case BOT_ELECTRICITY:
			return "Electricity";
		case BOT_MELEE:
			return "Melee";
	}
	return "Unknown";
}

int main(){
	puts(
		"Map:\n"
		"C T C T\n"
		". . . .\n"
		". . . .\n"
		"- - - -\n"
		". . . .\n"
		". . . .\n"
		"C T C T\n"
	);

	puts("Armors:");

	for (size_t i = 0; i < armors_length; i++) {
		printf(
			"%s"
			"\n\tSpeed %+d"
			"\n\tDamage %+d"
			"\n\tHealth %+d"
			"\n\tFire resistance %+d"
			"\n\tWater resistance %+d"
			"\n\tElectricity resistance %+d"
			"\n\tMelee resistance %+d"
			"\n\tFire bonus damage %+d"
			"\n\tWater bonus damage %+d"
			"\n\tElectricity bonus damage %+d"
			"\n\tMelee bonus damage %+d\n\n",
			armors[i].name,
			armors[i].speed,
			armors[i].attack,
			armors[i].health,
			armors[i].resistance.fire,
			armors[i].resistance.water,
			armors[i].resistance.electricity,
			armors[i].resistance.melee,
			armors[i].bonus.fire,
			armors[i].bonus.water,
			armors[i].bonus.electricity,
			armors[i].bonus.melee
		);
	}

	puts("Floorings:");

	for (size_t i = 0; i < floors_length; i++) {
		printf(
			"%s"
			"\n\tSpeed %+d"
			"\n\tFire damage %+d"
			"\n\tWater damage %+d"
			"\n\tElectricity damage %+d"
			"\n\tMelee damage %+d\n\n",
			floors[i].name,
			floors[i].speed,
			floors[i].hurt.fire,
			floors[i].hurt.water,
			floors[i].hurt.electricity,
			floors[i].hurt.melee
		);
	}

	Bot bob = {
		.type = BOT_ELECTRICITY,
		.speed = 1,
		.attack = 3,
		.health = 20,
		.resistance = {0},
		.bonus = {0}
	};

	puts("Bot stats example:");
	printf(
		"\tClass %s"
		"\n\tSpeed %+d"
		"\n\tDamage %+d"
		"\n\tHealth %+d"
		"\n\tFire resistance %+d"
		"\n\tWater resistance %+d"
		"\n\tElectricity resistance %+d"
		"\n\tMelee resistance %+d"
		"\n\tFire bonus damage %+d"
		"\n\tWater bonus damage %+d"
		"\n\tElectricity bonus damage %+d"
		"\n\tMelee bonus damage %+d\n\n",
		get_class(bob.type),
		bob.speed,
		bob.attack,
		bob.health,
		bob.resistance.fire,
		bob.resistance.water,
		bob.resistance.electricity,
		bob.resistance.melee,
		bob.bonus.fire,
		bob.bonus.water,
		bob.bonus.electricity,
		bob.bonus.melee
	);

	Map arena = {
		.top = {
			DIRT_FLOOR, DIRT_FLOOR, DIRT_FLOOR, DIRT_FLOOR,
			CONCRETE_FLOOR, CONCRETE_FLOOR, CONCRETE_FLOOR, CONCRETE_FLOOR
		},

		.middle = {
			CONDUCTIVE_FLOOR, LAVA_FLOOR, LAVA_FLOOR, CONDUCTIVE_FLOOR
		},

		.bottom = {
			SPIKE_FLOOR, ASPHALT_FLOOR, SPIKE_FLOOR, ASPHALT_FLOOR,
			WATER_FLOOR, CONDUCTIVE_FLOOR, LAVA_FLOOR, ASPHALT_FLOOR
		}
	};

	DrawMap(arena);
}
