#include <stdio.h>

#define ZONE_WIDTH 4
#define ZONE_HEIGHT 2

typedef struct damage Damage;
typedef struct equipment Equipment;
typedef struct bot Bot;
typedef struct floor Floor;
typedef struct map Map;

struct damage {
	int fire;
	int water;
	int electricity;
	int melee;
};

struct equipment {
	char *name;
	int speed;
	int health;
	int attack;
	Damage resistance;
	Damage bonus;
};

typedef enum bot_class {
	BOT_FIRE,
	BOT_WATER,
	BOT_ELECTRICITY,
	BOT_MELEE
} BotClass;

struct bot {
	BotClass type;
	int speed;
	int attack;
	int health;
	Damage resistance;
	Damage bonus;
};

struct floor {
	char *name;
	int speed;
	Damage hurt;
};

struct map {
	Floor top[ZONE_WIDTH*ZONE_HEIGHT];
	Floor middle[ZONE_WIDTH*1];
	Floor bottom[ZONE_WIDTH*ZONE_HEIGHT];
};

char *get_class(BotClass type);
