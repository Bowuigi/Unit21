#define FLAG_DESC_LEN 64

typedef char Flag;
typedef Flag * FlagList;

const char *Flag_Describe(Flag flag);
int Flag_Find(Flag flag, FlagList list);
