#include "flags.h"

char Flags_Default[] = "2aceor";

char Flag_Descriptions[255][FLAG_DESC_LEN] = {
	['+'] = "Show future events",
	['-'] = "Show past events",
	['a'] = "Show every event",
	['c'] = "Colors",
	['C'] = "No colors",
	['s'] = "Sort from priority A to Z",
	['r'] = "Sort from priority Z to A",
	['e'] = "Use DD MMMM YYYY HH:MM format for dates",
	['i'] = "Use YYYY-MM-DD HH:MM format for dates",
	['u'] = "Use MM/DD/YYYY HH:MM format for dates",
	['w'] = "Use DD/MM/YY HH:MM format for dates",
	['1'] = "12 hour format",
	['2'] = "24 hour format",
	['o'] = "Mark overlapping events",
	['O'] = "Do not mark overlapping events",
};

const char *Flag_Describe(Flag flag) {
	return Flag_Descriptions[flag];
}

int Flag_Find(Flag flag, FlagList list) {
	int index;
	for (index = 0; list[index] != flag; index++);

	return index;
}
