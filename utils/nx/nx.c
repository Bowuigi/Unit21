#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include "../../shared/parsing.h"
#include "../../shared/msg.h"

#define MAX_EV_NAME 512
#define MAX_FMT MAX_EV_NAME + 30
#define MAX_PRINT_DATE 512

#define ParseError(index, msg) {Messagef("NX", "error while parsing", "character %d, %s", index, msg); exit(1);}

typedef struct tm Date;

typedef struct event {
	char priority; // A-Z 
	Date date;
	char name[MAX_EV_NAME];
} Event;

static const char *months[] = {
	"January",
	"February",
	"March",
	"April",
	"May",
	"June",
	"July",
	"August",
	"September",
	"October",
	"November",
	"December"
};

static Event ParseLine(char *line) {
	// P YYYY-MM-DD HH:SS | Event name
	Event ev;
	char ext[MAX_FMT] = {0};
	size_t index = 0;

	SCopy(ext, line, MAX_FMT);

	if (InRange(ext[index], 'A', 'Z')) {
		ev.priority = ext[index];
	} else ParseError(index, "Invalid priority, use a character from A to Z");
	
	index++;

	if (!SkipWhitespace(ext, &index)) ParseError(index, "Expected whitespace");

	// Extract the ISO formatted date
	// Year
	int year = NextN(ext, &index, 4);
	
	if (year == -1) ParseError(index, "Invalid year, the date format is YYYY-MM-DD HH:MM");

	// Separator
	if (!SkipC(ext, &index, '-')) ParseError(index, "Invalid separator, the date format is YYYY-MM-DD HH:MM");

	// Month
	int month = NextN(ext, &index, 2);

	if (month == -1 || !InRange(month, 1, 12)) ParseError(index, "Invalid month, the date format is YYYY-MM-DD HH:MM");

	// Separator
	if (!SkipC(ext, &index, '-')) ParseError(index, "Invalid separator, the date format is YYYY-MM-DD HH:MM");

	// Day
	int day = NextN(ext, &index, 2);

	if (day == -1 || !InRange(day, 1,31)) ParseError(index, "Invalid day, the date format is YYYY-MM-DD HH:MM");

	// Whitespace
	if (!SkipWhitespace(ext, &index)) ParseError(index, "Expected whitespace");

	// Hour
	int hour = NextN(ext, &index, 2);
	
	if (hour == -1 || !InRange(hour, 0, 23)) ParseError(index, "Invalid hour, the date format is YYYY-MM-DD HH:MM");

	// Separator
	if (!SkipC(ext, &index, ':')) ParseError(index, "Invalid separator, the date format is YYYY-MM-DD HH:MM");

	// Minute
	int minute = NextN(ext, &index, 2);

	if (minute == -1 || !InRange(hour, 0, 59)) ParseError(index, "Invalid minute, the date format is YYYY-MM-DD HH:MM");

	// Separator
	(void) SkipWhitespace(ext, &index);

	if (!SkipC(ext, &index, '|')) ParseError(index, "Expected '|' after date and before event name");
	
	(void) SkipWhitespace(ext, &index);

	// Event name
	char *name = Next(ext, &index, MAX_EV_NAME);

	if (!name) ParseError(index, "Out of memory");

	SReplace(name, MAX_EV_NAME-1, '\n', '\0');

	SCopy(ev.name, name, MAX_EV_NAME-1);

	free(name);

	// Set fields
	ev.date.tm_year = year - 1900;
	ev.date.tm_mon = month - 1;
	ev.date.tm_mday = day;

	ev.date.tm_hour = hour;
	ev.date.tm_min = minute;
	ev.date.tm_sec = 0;

	ev.date.tm_isdst = -1;

	// Fix and complete the date struct
	mktime(&ev.date);

	return ev;
}

void PrintEvent(Event ev) {
	// Check how far it is from today in average standard days (24 hours)
	time_t days_left = difftime(mktime(&ev.date), time(NULL)) / (
			60 *// Seconds in a minute
			60 *// Minutes in an hour
			24  // Hours in a day
			) + 1;

	// Set format
	char time_str[MAX_PRINT_DATE] = {0};
	strftime(time_str, MAX_PRINT_DATE, "%A %d, %B %Y at %H:%M", &ev.date);
	
	printf("%s\n\tPriority: %c\n\tScheduled for: %s", ev.name, ev.priority, time_str);

	if (days_left > 0) {
		printf(" (in %ld day%s)\n\n", days_left, (days_left > 1) ? "s" : "");
	} else if (days_left == 0) {
		printf(" (today)\n\n");
	} else {
		printf(" (%ld day%s ago)\n\n", -days_left, (days_left < -1) ? "s" : "");
	}
}

int main(int argc, char **argv) {
	char line[MAX_FMT] = {0};
	Event ev = {0};
	FILE *fp;
	char *agenda = (argc > 1) ? (argv[1]) : "agenda.nx";

	fp = fopen(agenda, "r");

	if (!fp) {
		Messagef("NX", "fatal error while reading agenda", "%s, %s\n", agenda, strerror(errno));
		return 1;
	}

	while (fgets(line, MAX_FMT, fp) != NULL) {
		if (line[0] == '\n' || line[0] == '\0') continue; // Skip empty lines
		ev = ParseLine(line);
		PrintEvent(ev);
	}

	if (ferror(fp)) {
		Messagef("NX", "fatal error while reading agenda", "%s, %s\n", agenda, "IO error");
		return 1;
	}
}

