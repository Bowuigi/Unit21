# Future plans for NX

Eventually nx will become a full planner, with subcommands and all those nice things

Here is an example session from the future

```
$ nx list
NX, listing agendas
birthdays.nx
job.nx
travel.nx
payments.nx

$ nx show travel
NX, flags: '2aceor', showing events in travel.nx
Italy
	Priority: C
	Scheduled for 12 Jan 2023 07:30

Spain
	Priority: C
	Scheduled for 12 Jan 2024 12:30

$ nx edit job
[Add some more events to job]
$ nx flag w
NX, enabled flag w, which disabled flag e
$ nx flag
NX, listing flags
2: 24 hour format
a: Show every event
c: Colors
o: Mark overlapping events
r: Sort from priority Z to A
w: Use DD/MM/YY HH:MM format for dates
```

# Flags:

+ **+**: Show events that didn't happen (after right now)
+ **-**: Show events that happened (before right now)
+ **a**: Show every event (the default)
+ **c**: Show colors (the default)
+ **C**: Disable colors
+ **s**: Sort by priority (A-Z)
+ **r**: Reverse sort by priority (Z-A)
+ **e**: Use easy to read ISO like formatted dates (the default)
+ **i**: Use ISO formatted dates
+ **u**: Use United States' MM/DD/YYYY HH:MM formatted dates
+ **w**: Use DD/MM/YY HH:MM formatted dates like the rest of the World
+ **1**: Use 12 hour format
+ **2**: Use 24 hour format (the default)
+ **o**: Mark overlapping events (the default)
+ **O**: Do not mark overlapping events

# Subcommands:

+ **list**: List every agenda in ~/.config/nx/
+ **flag**: If no arguments are given, describe the current flags. Otherwise enable the flags given.
+ **edit**: Edit a specific agenda
+ **show**: Show the events of every agenda (if no arguments are given, default action) or of a specific one (if one argument is given)
+ **cal**: Render the events of the following 3 months in a calendar
