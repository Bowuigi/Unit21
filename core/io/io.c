/*
 * Unit21/Core/IO: Replacement for cat, tac, tr, shuf and more, with some extra basic file manipulation
*/

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include "../../shared/msg.h"
#include "../../shared/sargp.h"

#define USAGE \
"io [options] [filenames]\n\
	-- Stop handling arguments\n\
	-h Show this help\n\
	-i Input mode (work like cat, the default)\n\
	-o Output mode (work like tee instead of cat)\n\
	-O Append+Output mode (work like 'tee -a')\n\
	-s Silent Mode, silences the output of -o and -O\n\
	-1 Remove numbers from the given streams\n\
	-L Remove letters from the given streams\n\
	-C Remove control characters (except newlines) from the given streams\n\
	-F Remove newlines (flatten) from the given streams\n\
	-A Remove alphanumeric characters from the given streams\n\
	-S Remove spaces (except newlines) from the given streams\n\
	-P Remove symbols (punctuation) from the given streams\n"

#define ARG_PARSE \
	SARGP_Parse(argv[1]) { \
		case 'h': \
			Message("IO", "usage", USAGE);                                 \
			return 0;                                                      \
			break;                                                         \
		case 'c':                                                          \
			WorkMode = WMODE_CHAR;                                         \
			break;                                                         \
		case 'l':                                                          \
			WorkMode = WMODE_LINE;                                         \
			break;                                                         \
		case 'R':                                                          \
			Actions |= ACTION_RANDOM;                                      \
			break;                                                         \
		case 'u':                                                          \
			Actions |= ACTION_REVERSE;                                     \
			break;                                                         \
		case 'C':                                                          \
			Actions |= ACTION_RM_CONTROL;                                  \
			break;                                                         \
		case '1':                                                          \
			Actions |= ACTION_RM_NUMBERS;                                  \
			break;                                                         \
		case 'L':                                                          \
			Actions |= ACTION_RM_LETTERS;                                  \
			break;                                                         \
		case 'A':                                                          \
			Actions |= ACTION_RM_ALPHANUM;                                 \
			break;                                                         \
		case 'S':                                                          \
			Actions |= ACTION_RM_SPACES;                                   \
			break;                                                         \
		case 'P':                                                          \
			Actions |= ACTION_RM_PUNCT;                                    \
			break;                                                         \
		case 'F':                                                          \
			Actions |= ACTION_FLATTEN;                                     \
			break;                                                         \
		case 'i':                                                          \
		case '-':                                                          \
			IOMode = MODE_INPUT;                                           \
			break;                                                         \
		case 'o':                                                          \
			IOMode = MODE_OUTPUT;                                          \
			break;                                                         \
		case 'a':                                                          \
			IOMode = MODE_APPEND;                                          \
			break;                                                         \
		case 's':                                                          \
			Actions |= ACTION_SILENT;                                      \
			break;                                                         \
		case 'r':                                                          \
			Actions |= ACTION_REPLACE;                                     \
			break;                                                         \
		default:                                                           \
			Messagef("IO", "warning", "Unknown argument -%c\n", *argv[1]); \
			break;                                                         \
	} SARGP_End

#define FILTER_INPUT(ch) \
	if ((Actions & ACTION_RM_ALPHANUM) &&  isalnum(ch)              ) continue; \
	if ((Actions & ACTION_RM_CONTROL ) &&  iscntrl(ch) && ch != '\n') continue; \
	if ((Actions & ACTION_RM_LETTERS ) &&  isalpha(ch)              ) continue; \
	if ((Actions & ACTION_RM_NUMBERS ) &&  isdigit(ch)              ) continue; \
	if ((Actions & ACTION_RM_SPACES  ) &&  isspace(ch) && ch != '\n') continue; \
	if ((Actions & ACTION_RM_PUNCT   ) &&  ispunct(ch) && ch != '\n') continue; \
	if ((Actions & ACTION_FLATTEN    ) &&  ch == '\n'               ) continue;

enum {
	MODE_INPUT,
	MODE_OUTPUT,
	MODE_APPEND,
} IOMode;

enum {
	WMODE_LINE,
	WMODE_CHAR,
} WorkMode;

enum {
	ACTION_FLATTEN      = 1 << 0,
	ACTION_RANDOM       = 1 << 1,
	ACTION_REPLACE      = 1 << 2,
	ACTION_REVERSE      = 1 << 3,
	ACTION_RM_ALPHANUM  = 1 << 4,
	ACTION_RM_CONTROL   = 1 << 5,
	ACTION_RM_LETTERS   = 1 << 6,
	ACTION_RM_NUMBERS   = 1 << 7,
	ACTION_RM_PUNCT     = 1 << 8,
	ACTION_RM_SPACES    = 1 << 9,
	ACTION_SILENT       = 1 << 10,
} Actions;

bool Write(FILE *input, FILE *output) {
	int ch;

	while (1) {
		ch = fgetc(input);

		if (ch == EOF) {
			if (ferror(stdin)) {
				Message("IO", "fatal error", "Could not read file\n");
				return false;
			}
			break;
		}

		FILTER_INPUT(ch)

		fputc(ch, output);
	}

	return true;
}

bool Write2(FILE *input, FILE *output1, FILE *output2) {
	int ch;

	while (1) {
		ch = fgetc(input);

		if (ch == EOF) {
			if (ferror(stdin)) {
				Message("IO", "fatal error", "Could not read file\n");
				return false;
			}
			break;
		}

		FILTER_INPUT(ch)

		fputc(ch, output1);
		fputc(ch, output2);
	}

	return true;
}

int main(int argc, char **argv) {
	switch (argc) {
		case 1:
			// Grab stdin and output it to stdout like cat
			return ((Write(stdin, stdout) == true) ? 0 : 1);
			break;
		case 2:
			if (argv[1][0] == '-') {
				// Arguments
				ARG_PARSE

				return ((Write(stdin, stdout) == true) ? 0 : 1);
			} else {
				// File
				FILE *file = fopen(argv[1], "r");
				bool wstatus = false;

				if (!file) {
					Messagef("IO", "fatal error", "Could not read file '%s': %s\n", argv[1], strerror(errno));
					return 1;
				}

				wstatus = Write(file, stdout);

				fclose(file);

				if (wstatus == false) return 1;
			}
			break;
		default:
			if (argv[1][0] == '-') {
				// Arguments and files
				ARG_PARSE

				for (size_t i = 2; i < argc; i++) {
					FILE *file;
					bool wstatus = false;
					char mode[1] = "r";

					switch (IOMode) {
						case MODE_INPUT:
							mode[0] = 'r';
							break;
						case MODE_OUTPUT:
							mode[0] = 'w';
							break;
						case MODE_APPEND:
							mode[0] = 'a';
							break;
					}

					file = fopen(argv[i], mode);

					if (!file) {
						Messagef("IO", "fatal error", "Could not read file '%s': %s\n", argv[i], strerror(errno));
						return 1;
					}

					if (IOMode == MODE_INPUT) {
						wstatus = Write(file, stdout);
					} else {
						if (Actions & ACTION_SILENT) {
							wstatus = Write(stdin, file);
							Write(file, stdout);
						} else {
							wstatus = Write2(stdin, file, stdout);
						}
					}

					fclose(file);

					if (wstatus == false) return 1;
				}
			} else {
				// Just files
				for (size_t i = 1; i < argc; i++) {
					FILE *file = fopen(argv[i], "r");
					bool wstatus = false;

					if (!file) {
						Messagef("IO", "fatal error", "Could not read file '%s': %s\n", argv[i], strerror(errno));
						return 1;
					}

					wstatus = Write(file, stdout);

					fclose(file);

					if (wstatus == false) return 1;
				}
			}
			break;
	}
}

