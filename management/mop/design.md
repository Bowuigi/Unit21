# Mop

Command line options:

`-c` is clean (remove orphan packages)

`-u` is update

`-i` is install

`-I` is info

`-s` is search

`-C` is contains

`-l` is list (show all packages installed or in a specific repo)

More of the same option means retry on fail, for example, `-uuu` tries to update, on fail it tries again and then again.
