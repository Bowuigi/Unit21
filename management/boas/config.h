#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <limits.h>
#include <errno.h>
#include "../../shared/parsing.h"
#include "../../shared/c3g.h"

struct Config {
	C3_Node *allowed;
	C3_Node *disallowed;
	C3_Node *messages;
	C3_Node *_top;
};

struct Config *ReadConfig(char *filename);
void DeleteConfig(struct Config *cfg);
bool IsAllowed(struct Config *cfg, char *user, char *cmd);
char *GetMessage(struct Config *cfg, char *type, char *errname);

#endif // CONFIG_H header guard
