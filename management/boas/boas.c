// IO and checking
#include <stdio.h>
#include <string.h>

// Memory management
#include <stdlib.h>

// User and group handling
#include <pwd.h>
#include <grp.h>
#include <sys/wait.h>
#include <unistd.h>
#include <shadow.h>

// Signal handling
#include <signal.h>

// Error handling
#include <errno.h>

// Limits for allocating memory
#include <limits.h>

// Terminal handling
#include "../../shared/term.h"
#include "../../shared/colors.h"
#include "../../shared/parsing.h"

// Config parsing
#include "config.h"

#define PASSWORD_SIZE 1024

FILE *tty;
struct termios term;
struct termios orig_term;

void NoCfgErr(char *e) {
	if (C_ERR_ENABLED) { 
		fputs(e, stderr);
	} else {
		char *msg;
		msg = StripESC(e);
		fputs(msg, stderr);
		free(msg);
	}
}

void CfgErr(struct Config *cfg, char *err) {
	if (!err) return;

	char *tmp = GetMessage(cfg, "error", err);

	char *msg = PushC(tmp, '\n');
	free(tmp);

	if (C_ERR_ENABLED) {
		fputs(msg, stderr);
	} else {
		char *no_color;
		no_color = StripESC(msg);
		fputs(no_color, stderr);
		free(no_color);
	}

	free(msg);
}


void signal_handler(int sig) {
	(void) sig;

	Term_SetState(orig_term, tty);
	fclose(tty);

	fputc('\n', stderr);
	NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Interrupted authentication\n" C_RESET);

	exit(1);
}

static void set_perms(uid_t uid, gid_t gid) {
	setgid(gid);
	setegid(gid);
	setuid(uid);
	seteuid(uid);
}

int main(int argc, char **argv) {
	C_init();

	if (geteuid() != 0 || getegid() != 0) {
		NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: incorrect permissions, run 'make setuid' as root inside the Boas source to set correct permissions\n" C_RESET);
		return 1;
	}

	uid_t default_uid = getuid();
	gid_t default_gid = getgid();

	struct passwd *runner;
	struct spwd *runner_shadow;

	runner = getpwuid(getuid());
	runner_shadow = getspnam(runner->pw_name);

	struct Config *cfg = ReadConfig("/etc/boas.c3");

	if (!cfg) {
		switch (C3_Error) {
			case C3_OK:
			case C3_ERR_OOM:
				NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Out of memory\n" C_RESET);
				break;
			case C3_ERR_INDENT:
				NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Incorrect indentation\n" C_RESET);
				break;
			case C3_ERR_NAME_TOO_LONG:
				NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Node name too long\n" C_RESET);
				break;
			case C3_ERR_VALUE_TOO_LONG:
				NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Node value too long\n" C_RESET);
				break;
			case C3_ERR_MAX_DEPTH:
				NoCfgErr(C_RESET C_BOLD C_RED "[Boas]" C_RESET C_BOLD " Fatal error: Max C3 depth reached\n" C_RESET);
				break;
		}
		return 1;
	}

	// Check if the user is allowed to do what it wants to do
	bool allowed = false;

	if (argc == 1) {
		allowed = IsAllowed(cfg, runner->pw_name, "sh");
	} else {
		allowed = IsAllowed(cfg, runner->pw_name, argv[1]);
	}

	if (!allowed) {
		CfgErr(cfg, "Access denied, contact your administrator for more information");
		DeleteConfig(cfg);
		return 1;
	}

#ifdef DEBUG
	if (1) {
#else
	if (getuid() != 0 && getgid() != 0) {
#endif
		// Setup getting the password
		tty = fopen("/dev/tty", "r");

		if (!tty) {
			CfgErr(cfg, strerror(errno));
			DeleteConfig(cfg);
			set_perms(default_uid, default_gid);
		}

		Term_GetState(orig_term, tty);
		Term_GetState(term, tty);
		Term_RemoveFlag(term, ECHO);
		Term_AddFlag(term, ICANON | ECHOE | ECHOK);
		Term_SetState(term, tty);

		signal(SIGINT, signal_handler);

		int correct = 0;
		for (int attempts = 0; attempts < 3; attempts++) {
			// Print password prompt
			char *pw_prompt = GetMessage(cfg, "password", NULL);
			if (C_ERR_ENABLED) {
				fputs(pw_prompt, stderr);
			} else {
				char *no_color;
				no_color = StripESC(pw_prompt);
				fputs(no_color, stderr);
				free(no_color);
			}
			free(pw_prompt);

			char input[PASSWORD_SIZE] = {0};
			size_t size = 0;

			// TODO: Move to fgets?
			for (int c = getc(tty); c != EOF && c != '\n'; c = getc(tty)) {
				if (size >= PASSWORD_SIZE) break;
				input[size] = (char)c;
				size++;
			}

			// Add a newline to prevent messy output
			fputc('\n', stderr);

			if (*input == '\0') {
				CfgErr(cfg, "A password is required");
				correct = 0;
				continue;
			}

			correct = !strcmp(runner_shadow->sp_pwdp, crypt(input, runner_shadow->sp_pwdp));

			(void) memset(input, 0, PASSWORD_SIZE);

			if (!correct) {
				CfgErr(cfg, "Incorrect password");
			} else {
				break;
			}
		}

		signal(SIGINT, SIG_DFL);

		// Clean up
		Term_SetState(orig_term, tty);
		fclose(tty);

		if (!correct) {
			CfgErr(cfg, "Failed to authenticate with password");
			DeleteConfig(cfg);
			return 1;
		}
	}

	char **ex = calloc(argc+1, sizeof(char*));

	if (argc == 1) {
		ex[0] = getpwuid(0)->pw_shell;
	}

	for (int i = 1; i < argc; i++) {
		ex[i-1] = argv[i];
	}

	// CRITICAL SECURITY POINT, code after this line MUST NOT CRASH, RESET PERMS ON EXIT
	set_perms(0, 0);

	pid_t pid = fork();

	switch (pid) {
		case -1: // Error
			free(ex);
			CfgErr(cfg, strerror(errno));
			DeleteConfig(cfg);
			set_perms(default_uid, default_gid);
			return 1;
			break;
		case 0: // Child process
			errno = 0;
			execvp(ex[0], ex);

			set_perms(default_uid, default_gid);

			switch (errno) {
				case EACCES:
					CfgErr(cfg, "Permission denied");
					break;
				case ENOENT:
					CfgErr(cfg, "Executable not found");
					break;
				case ENOEXEC:
					CfgErr(cfg, "File is not an executable");
					break;
				default:
					CfgErr(cfg, strerror(errno));
					break;
			}

			DeleteConfig(cfg);
			return -errno;

			break;
		default: { // Parent process
			set_perms(default_uid, default_gid);
			pid_t status;
			pid_t finished = wait(&status);

			free(ex);

			if (finished != pid) {
				set_perms(default_uid, default_gid);
				CfgErr(cfg, "Got PID of unknown process, expected PID of child");
				DeleteConfig(cfg);
				return 1;
			}

			if (WIFEXITED(status)) {
				set_perms(default_uid, default_gid);
				DeleteConfig(cfg);
				return WEXITSTATUS(status);
			}

			if (WIFSIGNALED(status)) {
				set_perms(default_uid, default_gid);
				DeleteConfig(cfg);
				return WTERMSIG(status);
			}

			DeleteConfig(cfg);
			return status;
		}
	}
}
