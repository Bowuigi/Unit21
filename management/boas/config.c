#include "config.h"

struct Config *ReadConfig(char *filename) {
	FILE *fp = fopen(filename, "r");
	C3_Node *top = C3_ParseFile(fp);
	fclose(fp);

	if (C3_Error != C3_OK) return NULL;

	struct Config *cfg = malloc(sizeof(struct Config));

	if (!cfg) return NULL;

	cfg->_top = top;
	cfg->allowed = C3g_Get(top->child, "Allowed");
	cfg->disallowed = C3g_Get(top->child, "Disallowed");
	cfg->messages = C3g_Get(top->child, "Messages");

	return cfg;
}

void DeleteConfig(struct Config *cfg) {
	C3_DestroyNode(cfg->_top, C3_DESTROY_NEXT, C3_DESTROY_CHILD);
	free(cfg);
}

bool IsAllowed(struct Config *cfg, char *user, char *cmd) {
	if (!cfg) return false;
	if (!cfg->allowed || !user || !cmd) return false;

	C3_Node *name = C3g_Get(cfg->allowed->child, user);

	if (cfg->disallowed && C3g_Get(cfg->disallowed->child, user)) return false;

	C3_Node *agroup = NULL;
	C3_Node *dgroup = NULL;

	if (!name) {
		long max_groups = sysconf(_SC_NGROUPS_MAX) + 1;
		gid_t *groups = malloc(max_groups * sizeof(gid_t));
		int ngroups = getgroups(max_groups, groups);
		struct group *grp;

		if (ngroups == -1) return false;

		for (int i = 0; i < ngroups; i++) {
			grp = getgrgid(groups[i]);

			char *gname = malloc((strlen(grp->gr_name) + 2) * sizeof(char));

			if (!gname) return false;

			strcpy(gname+1, grp->gr_name);
			gname[0] = '@';

			agroup = C3g_Get(cfg->allowed->child, gname);
			if (cfg->disallowed) dgroup = C3g_Get(cfg->disallowed->child, gname);

			free(gname);
			if (agroup && !dgroup) break;
		}

		free(groups);

		if (!agroup) return false;

		return (agroup->child == NULL || C3g_Find(agroup->child, cmd) != 0);
	}

	return (name->child == NULL || C3g_Find(name->child, cmd) != 0);
}

// Local macro for convenience
// 2 is substracted from dest_i:
// One is to remove the NUL, the other is because strings start from 0, lengths start from 1
#define to_xmsg(s) {xmsg = SRCopy(xmsg, &xmsg_len, &dest_i, dest_i, s, strlen(s)); dest_i -= 2;}
char *GetMessage(struct Config *cfg, char *type, char *errname) {
	if (!cfg || !type) return NULL;

	// Get username
	struct passwd *p = getpwuid(getuid());
	if (!p) return "[User does not exist]";
	char *username = p->pw_name;

	// Get hostname
	char hostname[HOST_NAME_MAX+1] = {0};
	if (gethostname(hostname, HOST_NAME_MAX) == -1) return strdup("[Unknown hostname]");

	// Color template
	char *color = strdup("\033[31m");

	// Attribute template
	char *attr = strdup("\033[0m");

	if (!errname) errname = "Success";

	// Get node
	C3_Node *n = C3g_Get(cfg->messages->child, type);
	if (n == NULL) {
		free(color);
		free(attr);
		return strdup("[Missing message]");
	}

	// Node name
	char *msg = n->value;
	if (!msg || !(*msg)) {
		free(color);
		free(attr);
		return strdup("[No message]");
	}
	size_t msg_len = strlen(msg);

	size_t xmsg_len = 8;
	char *xmsg = malloc(sizeof(char) * xmsg_len);

	if (!xmsg) {
		free(color);
		free(attr);
		return strdup("[Out of memory]");
	}

	bool special = false;

	size_t src_i = 0, dest_i = 0;

	for (; src_i <= msg_len; src_i++, dest_i++) {
		if (dest_i >= xmsg_len) {
			xmsg_len <<= 1;
			char *tmp = realloc(xmsg, xmsg_len * sizeof(char));
			if (!tmp) {
				free(color);
				free(attr);
				return strdup("[Out of memory]");
			}
			xmsg = tmp;
		}

		if (special) {
			special = false;
			switch (msg[src_i]) {
				// Special
				case 'u': // Username
					to_xmsg(username);
					break;
				case 'h': // Hostname
					to_xmsg(hostname);
					break;
				case 'e': // Error
					if (errname)
						to_xmsg(errname);
					break;
				// Colors
				case 'r': // Red
					color[3] = '1';
					to_xmsg(color);
					break;
				case 'g': // Green
					color[3] = '2';
					to_xmsg(color);
					break;
				case 'b': // Blue
					color[3] = '4';
					to_xmsg(color);
					break;
				case 'y': // Yellow
					color[3] = '3';
					to_xmsg(color);
					break;
				case 'm': // Magenta
					color[3] = '5';
					to_xmsg(color);
					break;
				case 'c': // Cyan
					color[3] = '6';
					to_xmsg(color);
					break;
				case 'w': // White
					color[3] = '7';
					to_xmsg(color);
					break;
				// Attributes
				case 'B': // Bold
					attr[2] = '1';
					to_xmsg(attr);
					break;
				case 'R': // Reverse
					attr[2] = '7';
					to_xmsg(attr);
					break;
				case 'N': // Normal
					attr[2] = '0';
					to_xmsg(attr);
					break;
				// Everything else
				default:
					if (src_i == msg_len)
						xmsg[dest_i] = '\0';
					else
						xmsg[dest_i] = msg[src_i];
					break;
			}
		} else {
			if (msg[src_i] == '%') {
				special = true;
				dest_i--;
			} else if (src_i == msg_len) {
				xmsg[dest_i] = '\0';
			} else {
				xmsg[dest_i] = msg[src_i];
			}
		}
	}

	free(color);
	free(attr);
	return xmsg;
}
#undef to_xmsg

