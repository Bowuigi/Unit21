#include <unistd.h>
#include <pwd.h>
#include <grp.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include "../../shared/colors.h"

#define eputs(err) if (C_ENABLED) { fputs(C_RESET C_BOLD C_RED err "\n", stderr); } else { fputs(err"\n", stderr); }

static void print_uinfo(struct passwd *usr) {
	errno = 0;
	struct group *grp = getgrgid(usr->pw_gid);

	if (!grp) {
		switch (errno) {
			case ENOMEM:
				eputs("Failed to get default group information: Out of memory")
				break;
			case EIO:
				eputs("Failed to get default group information: IO error")
				break;
			default:
				eputs("Failed to get default group information: Group not found")
				break;
		}
	}

	if (C_ENABLED) {
		printf(
				C_RESET "User: " C_BOLD C_BLUE "%s\n"
				C_RESET "UID: "C_BOLD C_BLUE "%d\n"
				C_RESET "Default GID: "C_BOLD C_BLUE "%d "C_RESET"("C_BOLD C_BLUE"%s"C_RESET")\n"
				C_RESET "Home directory: "C_BOLD C_BLUE "%s\n"
				C_RESET "Login shell: "C_BOLD C_BLUE "%s\n",
				usr->pw_name, usr->pw_uid, usr->pw_gid, grp->gr_name, usr->pw_dir, usr->pw_shell
			);
	} else {
		printf(
				"User: %s\n"
				"UID: %d\n"
				"Default GID: %d (%s)\n"
				"Home directory: %s\n"
				"Login shell: %s\n",
				usr->pw_name, usr->pw_uid, usr->pw_gid, grp->gr_name, usr->pw_dir, usr->pw_shell
			);
	}

	size_t groups = 0;
	size_t mem = 0;
	int grp_amount = 0;
	struct group *tmpgrp = getgrent();

	for (groups = 0; tmpgrp != NULL; tmpgrp = getgrent(), groups++) {
		for (mem = 0; tmpgrp->gr_mem[mem] != NULL; mem++) {
			if (!strcmp(tmpgrp->gr_mem[mem], usr->pw_name)) {
				if (grp_amount == 0) {
					if (C_ENABLED) {
						printf(C_RESET "Supplementary groups:\n" C_BOLD C_BLUE);
					} else {
						printf("Supplementary groups:\n");
					}
				}
				printf("%s (%d)\n", tmpgrp->gr_name, tmpgrp->gr_gid);
				grp_amount++;
				break;
			}
		}
	}

	if (C_ENABLED) {
		fputs(C_RESET, stdout);
	}
}

static void handle_err(int err, char *username) {
	if (C_ENABLED) {
		switch (err) {
			case EIO:
				fprintf(stderr, C_RESET C_BOLD C_RED "Could not get user '%s' information: IO error\n", username);
				break;
			case ENOMEM:
				fprintf(stderr, C_RESET C_BOLD C_RED "Could not get user '%s' information: Out of memory\n", username);
				break;
			default:
				fprintf(stderr, C_RESET C_BOLD C_RED "Could not get user '%s' information: Not found\n", username);
				break;
		}
	} else {
		switch (err) {
			case EIO:
				fprintf(stderr, "Could not get user '%s' information: IO error\n", username);
				break;
			case ENOMEM:
				fprintf(stderr, "Could not get user '%s' information: Out of memory\n", username);
				break;
			default:
				fprintf(stderr, "Could not get user '%s' information: Not found\n", username);
				break;
		}
	}
}

int main(int argc, char **argv) {
	struct passwd *usr;

	C_init();

	switch (argc-1) {
		case 0:
			errno = 0;
			usr = getpwuid(getuid());
			if (usr == NULL) {
				handle_err(errno, "current user");
				return 1;
			}
			print_uinfo(usr);
			break;
		case 1:
			usr = getpwnam(argv[1]);
			if (usr == NULL) {
				handle_err(errno, argv[1]);
				return 1;
			}
			print_uinfo(usr);
			break;
		default:
			for (int i = 1; i < argc; i++) {
				usr = getpwnam(argv[i]);
				if (usr == NULL) {
					handle_err(errno, argv[i]);
					return 1;
				}
				print_uinfo(usr);
				if (i != argc-1) {
					printf("\n");
				}
			}
	}
	return 0;
}
