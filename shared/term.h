#ifndef TERM_H
#define TERM_H

#include <termios.h>
#include <stdio.h>

#define Term_GetState(term, file) tcgetattr(fileno(file), &term)

#define Term_SetState(term, file) tcsetattr(fileno(file), 0, &term)

#define Term_AddFlag(term, flag) term.c_lflag |= (tcflag_t)( flag )

#define Term_RemoveFlag(term, flag) term.c_lflag &= ~(tcflag_t)( flag )

#endif // TERM_H header guard
