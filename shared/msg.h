#ifndef MSG_H
#define MSG_H

#include <stdarg.h>
#include <stdio.h>

#define FMT_STR_LEN 512

void Message(const char *program, const char *context, const char *message);

void Messagef(const char *program, const char *context, const char *format, ...);

#endif // MSG_H header guard
