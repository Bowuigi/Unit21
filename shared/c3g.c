#include "c3g.h"

unsigned int C3g_Find(C3_Node *n, char *key) {
	if (n == NULL) return 0;

	for (unsigned int i = 1;; i++, n = n->next) {
		if (!strcmp(key, n->name)) {
			return i;
		};

		if (n->next == NULL) {
			return 0;
		}
	}
}

C3_Node *C3g_Index(C3_Node *n, unsigned int index) {
	for (unsigned int i = 1; n && i < index; i++, n = n->next);

	return n;
}

C3_Node *C3g_Get(C3_Node *n, char *key) {
	unsigned int i = C3g_Find(n, key);

	if (i == 0) return NULL;

	return C3g_Index(n, i);
}
