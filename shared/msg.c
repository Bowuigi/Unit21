#include "msg.h"

void Message(const char *program, const char *context, const char *message) {
	fprintf(stderr, "%s, %s: %s\n", program, context, message);
}

void Messagef(const char *program, const char *context, const char *format, ...) {
	va_list args;
	char fmt_str[FMT_STR_LEN] = {0};

	va_start(args, format);

	snprintf(fmt_str, FMT_STR_LEN, "%s, %s: ", program, context);

	size_t off;
	for (off = 0; fmt_str[off] != '\0'; off++);

	vsnprintf(fmt_str+off, FMT_STR_LEN-off, format, args);

	fputs(fmt_str, stderr);

	va_end(args);
}

