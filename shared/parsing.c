#include "parsing.h"

// Safe replacement for strncpy(3p) that always NUL terminates the copied string
char *SCopy(char *dest, char *src, size_t dest_len, size_t n) {
	if (!dest || !src) return dest;

	size_t i = 0;
	size_t s = dest_len;
	size_t min = 0;

	if (s < n) {
		min = s;
	} else {
		min = n;
	}

	for (i = 0; i < min; i++) {
		dest[i] = src[i];
	}

	dest[min] = '\0';

	return dest;
}

// Safe copy that resizes the dest string as needed, the dest string is always NUL terminated
char *SRCopy(char *dest, size_t *dest_size, size_t *dest_len, size_t dest_offset, char *src, size_t n) {
	if (!dest || !src) return NULL;

	char *str = dest;
	char *tmp = str;
	size_t i = 0;

	for (; i <= n; i++) {
		if (dest_offset+i >= (*dest_size)) {
			(*dest_size) <<= 1;
			tmp = realloc(str, sizeof(char) * (*dest_size));
			if (!tmp) return NULL;
			str = tmp;
		}

		if (i == n) {
			str[dest_offset+i] = '\0';
		} else {
			str[dest_offset+i] = src[i];
		}
	}

	(*dest_len) += i;

	return str;
}

void SReplace(char *str, size_t length, char rm, char repl) {
	size_t cur;

	for (cur = 0; cur < length; cur++) {
		if (str[cur] == rm) {
			str[cur] = repl;
		}
	}
}

// Generic function to skip characters on a string
// Returns 0 if 'c' was not found, 1 otherwise
int SkipC(char *str, size_t *index, char c) {
	if (str[*index] != c) return 0;

	for (; str[*index] == c; (*index)++);

	return 1;
}

// Get the next 'len' characters from 'sptr', incrementing the pointer
// Returns NULL on error, a NUL terminated string otherwise
char *Next(char *str, size_t *index, size_t len) {
	char *tmp = malloc(sizeof(char) * (len + 1));

	if (!tmp) return NULL;

	SCopy(tmp, str + (*index), len, len);

	*index += len;

	return tmp;
}

// Same as Next, but return the string converted to a number or -1 on error
long NextN(char *str, size_t *index, size_t len) {
	if (!str) return -1;

	char *extr = Next(str, index, len);

	if (!extr) return -1;

	char *endptr;

	long n = strtol(extr, &endptr, 10);

	if (extr == endptr || *endptr != '\0') return -1;

	free(extr);

	return n;
}

char *StripESC(char *msg) {
	size_t len = strlen(msg);

	char *s = malloc(sizeof(char) * len);
	size_t mi = 0, si = 0;

	while (mi<len) {
		if (msg[mi] == '\033') {
			mi++;
			if (msg[mi] == '[') mi++;

			for (;
				(msg[mi] >= '0' && msg[mi] <= '9') || // Numbers
				(msg[mi] == ';' || msg[mi] == ',')    // Separators
			; mi++);

			if ((msg[mi] >= 'A' && msg[mi] <= 'Z') ||
				(msg[mi] >= 'a' && msg[mi] <= 'z')) mi++; // Letter
		} else {
			s[si] = msg[mi];
			si++;
			mi++;
		}
	}

	return s;
}

char *PushC(char *s, char c) {
	size_t len = strlen(s);
	char *tmp = malloc((len+2) * sizeof(char));
	if (!tmp) return s;

	tmp[0] = '\0';
	SCopy(tmp, s, len, len);

	tmp[len+1] = '\0';
	tmp[len] = c;
	return tmp;
}

