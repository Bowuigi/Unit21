#ifndef C3G_H
#define C3G_H

#include <stdio.h>
#include "C3/c3.h"

unsigned int C3g_Find(C3_Node *n, char *key);

C3_Node *C3g_Index(C3_Node *n, unsigned int index);

C3_Node *C3g_Get(C3_Node *n, char *key);

#endif // C3G_H header guard
