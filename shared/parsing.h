#ifndef PARSING_H
#define PARSING_H

#include <assert.h>
#include <string.h>
#include <stdlib.h>

#define MAX_NEXT_SIZE 1024

char *SCopy(char *dest, char *src, size_t dest_len, size_t n);
char *SRCopy(char *dest, size_t *dest_size, size_t *dest_len, size_t dest_offset, char *src, size_t n);

void SReplace(char *str, size_t length, char rm, char repl);

#define InRange(n, min, max) ((n) >= (min) && (n) <= (max))

int SkipC(char *str, size_t *index, char c);

#define SkipWhitespace(str, index) (SkipC(str, index, ' ') + SkipC(str, index, '\t') + SkipC(str, index, '\n'))

char *Next(char *str, size_t *index, size_t len);

long NextN(char *str, size_t *index, size_t len);

char *StripESC(char *msg);

char *PushC(char *s, char c);

#endif // PARSING_H header guard
