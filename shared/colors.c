#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include "colors.h"

int C_NO_COLOR_DISABLED;
int C_COLOR_ALWAYS;
int C_TTY_ERR;
int C_TTY_OUT;

void C_init() {
	C_NO_COLOR_DISABLED = getenv("NO_COLOR") == NULL;
	char *color = getenv("COLOR");
	if (color != NULL) {
		C_COLOR_ALWAYS = strcmp(color, "always") == 0;
	} else {
		C_COLOR_ALWAYS = 0;
	}

	C_TTY_ERR = isatty(STDERR_FILENO) == 1;
	C_TTY_OUT = isatty(STDOUT_FILENO) == 1;
}
