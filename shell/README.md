Simple shell that has essencial features only, the rest can be done by shell commands

```sh
# Example of what is possible
command $EXPAND_VARIABLE | pipe ~/expand_tilde && and || or < read_from > send_to >> append_to 2> send_stderr &> send_stdout_and_stderr 2>> append stderr &>> append_stdout_and_stderr
```

The parsing is done with tokens, tokens are words that contain or start with specific characters, if a word doesn't match any token, it is converted into a string:

Tokens

- Pipe `|` (literal)
- And `&&` (literal)
- Or `||` (literal)
- Read from file `<` (literal + filename on next token)
- Write to file `>` (literal + filename on next token)
- Append to file `>>` (literal + filename on next token)
- Write standard error to file `2>` (literal + filename on next token)
- Write standard input and standard error to file `&>` (literal + filename on next token)
- Append standard error to file `2>>` (literal + filename on next token)
- Append standard input and standard error to file `&>>` (literal + filename on next token)
- Variable `$` (literal + string on the same token)
- Home directory `~` (literal + path on the same token)

The rest is marked as a string, use `'...'` to escape every character on a string, including spaces.

