MAKE=make

build:
	cd management/user; $(MAKE)
	cd management/group; $(MAKE)
	cd utils/xv; $(MAKE)
	cd management/boas; $(MAKE)

install:
	cd management/user; $(MAKE) install
	cd management/group; $(MAKE) install
	cd utils/xv; $(MAKE) install
	cd management/boas; $(MAKE) install

uninstall:
	cd management/user; $(MAKE) uninstall
	cd management/group; $(MAKE) uninstall
	cd utils/xv; $(MAKE) uninstall
	cd management/boas; $(MAKE) uninstall

clean:
	cd management/user; $(MAKE) clean
	cd management/group; $(MAKE) clean
	cd utils/xv; $(MAKE) clean
	cd management/boas; $(MAKE) clean

man:
	cd management/user; $(MAKE) man
	cd management/group; $(MAKE) man
	cd utils/xv; $(MAKE) man
	cd management/boas; $(MAKE) man
